<?php

namespace quoma\arya\zone\controllers;

use quoma\arya\zone\models\Zone;
use quoma\arya\zone\models\ZoneMember;
use quoma\core\web\Controller;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * ZoneController implements the CRUD actions for Zone model.
 */
class ZoneController extends Controller
{
    public function behaviors()
    {
        return array_merge(parent::behaviors(),[
        ]);
    }

    /**
     * Lists all Zone models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Zone::find()->orderBy('name')
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Zone model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Zone model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Zone();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->zone_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'membersDataProvider' => null
            ]);
        }
    }

    /**
     * Updates an existing Zone model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $membersDataProvider = new ActiveDataProvider([
            'query' => $model->getZoneMembers()
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->zone_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'membersDataProvider' => $membersDataProvider
            ]);
        }
    }

    /**
     * Deletes an existing Zone model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Zone model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Zone the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Zone::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     *
     * @param int $id
     * @return json
     */
    public function actionAddMember($id)
    {
        $model = $this->findModel($id);
        $member = new ZoneMember();
        $member->load(Yii::$app->request->post());

        if($member->validate() && $member->save()){

        }

        return $this->actionUpdate($id);
    }

    /**
     * Deletes an existing AccountConfig model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteMember($zone_id, $zone_member_id)
    {
        $modelDelete = ZoneMember::findOne([
            'zone_member_id'=> $zone_member_id]);
        if(!empty($modelDelete)) {
            $modelDelete->delete();
        }

        return $this->actionUpdate($zone_id);
    }

    public function actionZoneByName($name)
    {
        if (Yii::$app->request->isAjax) {
            $zones = Zone::searchByName($name);
            $response = array();
            foreach ($zones as $zone) {
                $response['results'][] = ['id' => $zone->zone_id, 'text' => $zone->name];
            }
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $response;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}