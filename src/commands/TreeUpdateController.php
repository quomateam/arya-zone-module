<?php

namespace quoma\arya\zone\commands;

use quoma\arya\zone\models\Location;
use Yii;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * Class ZoneController
 * Se actualizan todas las zonas desde los nodos padres.
 * @package app\commands
 */
class TreeUpdateController extends Controller
{

    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionIndex()
    {
        //Intro
        $this->stdout('Arya[Zone]', Console::BOLD, Console::FG_CYAN);
        $this->stdout(" | Command to update the location tree \n\n", Console::BOLD);
        echo "Starting... \n";

        Location::updateTree(0);
        $this->stdout("\nProccess successfully finished. \n", Console::BOLD, Console::FG_GREEN);
    }

}
