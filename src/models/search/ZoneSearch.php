<?php

namespace quoma\arya\zone\models\search;

use quoma\arya\zone\models\Zone;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use quoma\arya\zone\models\Location;

/**
 * LocationSearch represents the model behind the search form about `quoma\arya\zone\models\Location`.
 */
class ZoneSearch extends Zone
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Zone::find();

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['zone_id' => $this->zone_id]);
        $query->andFilterWhere(['like', 'name', $this->name]);
        $query->orderBy('name');

        return $query;
    }
}
