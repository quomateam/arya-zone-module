<?php
/**
 * Created by PhpStorm.
 * User: cgarcia
 * Date: 27/12/16
 * Time: 16:48
 */

namespace quoma\arya\zone\models;

use quoma\arya\zone\ZoneModule;


/**
 * This is the model class for table "zone_member".
 *
 * @property integer $zone_member_id
 * @property integer $location_id
 * @property integer $zone_id
 *
 * @property Location $location
 * @property Zone $zone
 */
class ZoneMember extends \quoma\core\db\ActiveRecord
{


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zone_member';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['location_id', 'zone_id'], 'integer'],
            [['location_id', 'zone_id'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'zone_member_id' => ZoneModule::t('Zone Member ID'),
            'location_id' => ZoneModule::t('Location'),
            'zone_id' => ZoneModule::t('Zone'),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(Location::className(), ['location_id' => 'location_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZone()
    {
        return $this->hasOne(Zone::className(), ['zone_id' => 'zone_id']);
    }


    /**
     * @inheritdoc
     * Strong relations: None.
     */
    public function getDeletable()
    {
        return true;
    }

    /**
     * @brief Deletes weak relations for this model on delete
     * Weak relations: Model.
     */
    protected function unlinkWeakRelations(){
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            if($this->getDeletable()){
                $this->unlinkWeakRelations();
                return true;
            }
        } else {
            return false;
        }
    }

}