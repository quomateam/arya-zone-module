<?php

namespace quoma\arya\zone\models;

use quoma\core\db\ActiveRecord;
use Yii;
use yii\db\conditions\LikeCondition;
use yii\helpers\StringHelper;

/**
 * This is the model class for table "location".
 *
 * @property integer $location_id
 * @property string $name
 * @property integer $parent_id
 * @property integer $postal_code
 * @property integer $create_timestamp
 * @property integer $update_timestamp
 * @property string $status
 * @property string $type
 * @property integer $lft
 * @property integer $rgt
 *
 * @property Location $parent
 * @property Location[] $locations
 */
class Location extends ActiveRecord
{

    const CACHE_TIME = 120;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'location';
    }

    public function behaviors()
    {
        return [
            'slug' => [
                'class' => \yii\behaviors\SluggableBehavior::className(),
                'slugAttribute' => 'system',
                'attribute' => 'name',
                'ensureUnique' => true
            ],
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['create_timestamp'],
                    \yii\db\ActiveRecord::EVENT_AFTER_UPDATE => ['update_timestamp'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'type', 'status'], 'required'],
            [['parent_id', 'create_timestamp', 'update_timestamp', 'postal_code', 'lft', 'rgt'], 'integer'],
            [['status', 'type'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['parent_id'], 'checkParent']
        ];
    }
    
    public function checkParent()
    {
        if($this->isNewRecord){
            return true;
        }
        
        if($this->parent && $this->parent->lft >= $this->lft && $this->parent->rgt <= $this->rgt){
            $this->addError('parent_id', Yii::t('app','Invalid parent location.'));
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'location_id' => \quoma\arya\zone\ZoneModule::t('ID'),
            'name' => \quoma\arya\zone\ZoneModule::t('Name'),
            'type' => \quoma\arya\zone\ZoneModule::t('Type'),
            'parent_id' => \quoma\arya\zone\ZoneModule::t('Parent'),
            'create_timestamp' => \quoma\arya\zone\ZoneModule::t('Create Timestamp'),
            'update_timestamp' => \quoma\arya\zone\ZoneModule::t('Update Timestamp'),
            'status' => \quoma\arya\zone\ZoneModule::t('Status'),
            'parent' => \quoma\arya\zone\ZoneModule::t('Parent'),
            'postal_code' => \quoma\arya\zone\ZoneModule::t('Postal Code'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Location::className(), ['location_id' => 'parent_id'])->cache(static::CACHE_TIME);
    }

    /**
     * @deprecated since 1.3
     * @return \yii\db\ActiveQuery
     */
    public function getLocations()
    {
        return $this->hasMany(Location::className(), ['parent_id' => 'location_id']);
    }

    /**
     * Devuelve los hijos
     * @return \yii\db\ActiveQuery
     */
    public function getChildren()
    {
        return $this->hasMany(Location::className(), ['parent_id' => 'location_id'])->orderBy(['name' => SORT_ASC])->cache(static::CACHE_TIME);
    }

    /**
     * @inheritdoc
     * Strong relations: None.
     */
    public function getDeletable()
    {
        if ($this->getLocations()->exists()) {
            return false;
        }

        return true;
    }

    /**
     * @brief Deletes weak relations for this model on delete
     * Weak relations: Parent, Locations.
     */
    protected function unlinkWeakRelations()
    {
        
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            if ($this->getDeletable()) {
                $this->unlinkWeakRelations();
                return true;
            }
        } else {
            return false;
        }
    }

    public function getTabName()
    {
        //Establece una sangría por nivel de Padres de Categoria
        $indent = '&nbsp;&nbsp;&nbsp;&nbsp;';

        // igualo $parent al padre
        $parent = $this->parent;
        // si $parent esta vació es decir no tiene padre el name queda igual SINO le agrega una sangría con $indent
        $name = empty($parent) ? $this->name : $indent . $this->name;

        if (!empty($parent)) {
            do {
                if ($parent = $parent->parent)
                    $name = $indent . $name; //le agrego otra sangría
            }
            while (!empty($parent)); // mientras tenga padre
        }

        return $name;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
    }

    public function beforeSave($insert)
    {
        // Si tiene padre busco el ultimo hijo para sacar rgt
        if ($this->parent) {
            $lastChild = $this->parent->getLocations()->orderBy(['location_id' => SORT_DESC])->one();
            $rgt = ($lastChild ? $lastChild->rgt : $this->parent->lft );
            // Actualizo todos los nodos
            Yii::$app->db->createCommand('UPDATE location SET rgt = rgt+2 WHERE rgt > ' . $rgt)->execute();
            Yii::$app->db->createCommand('UPDATE location SET lft = lft+2 WHERE lft > ' . $rgt)->execute();
            $this->lft = $rgt + 1;
            $this->rgt = $rgt + 2;
        } else {
            // Si no hay padre busco algun hermano
            $sibling = Location::find()->where('parent_id is null')->orderBy(['location_id' => SORT_DESC])->one();
            $rgt = ($sibling ? $sibling->rgt : 0 );
            $this->lft = $rgt + 1;
            $this->rgt = $rgt + 2;
        }
        return true;
    }

    public function afterDelete()
    {
        parent::afterDelete();

        $width = $this->rgt - $this->lft + 1;
        // Borro todos los hijos.
        Yii::$app->db->createCommand('DELETE FROM location WHERE lft BETWEEN ' . $this->lft . " AND " . $this->rgt)->execute();

        Yii::$app->db->createCommand('UPDATE location SET rgt = rgt - ' . $width . " WHERE rgt > " . $this->rgt)->execute();
        Yii::$app->db->createCommand('UPDATE location SET lft = lft - ' . $width . " WHERE lft > " . $this->rgt)->execute();
    }

    public function getFullLocation()
    {
        $location_aux = $this->name;
        $parent = $this->parent;
        if($parent) {
            do {
                $location_aux = $location_aux . ', ' . $parent->name;
                $parent = $parent->parent;
            } while ($parent);
        }

        return $location_aux;
    }

    public function getFullLocationIds()
    {
        $location_aux = [$this->location_id];
        $parent = $this->parent;
        if($parent) {
            do {
                $location_aux[] = $parent->location_id;
                $parent = $parent->parent;
            } while ($parent);
        }

        return $location_aux;
    }

    /**
     * Retorna las zonas para ser listadas en los selects
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getForSelect($except)
    {
        $query = Location::find()
            ->select(["node.location_id", "node.parent_id", "node.name", "CONCAT( REPEAT('&nbsp;&nbsp;', COUNT(parent.name) - 1), node.name ) as name", "(COUNT(parent.name) - 1) AS level", "node.lft", "node.rgt"])
            ->from(['location AS node'])
            ->innerJoin(['location AS parent'], 'node.lft BETWEEN parent.lft AND parent.rgt')
            ->groupBy(['node.name', 'node.parent_id', 'node.location_id'])
            ->orderBy('node.lft');
        
        //Ubicaciones hijas no se pueden seleccionar como padre
        if($except && $except->lft && $except->rgt){
            $query->andWhere('node.lft<'.$except->lft.' OR node.rgt>'.$except->rgt);
        }
        
        return $query->all();
    }

    /**
     * Busca Ubicación por nombre
     * @param $name
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function searchByName($name)
    {
        $query = Location::find();

        $query->filterWhere(['like', 'name', $name]);
        $query->andFilterWhere(['status' => 'enabled']);

        return $query->all();
    }

    /**
     * Busca Ubicación por nombre
     * @param $name
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function searchTypeByName($type = null, $name = null)
    {
        $query = Location::find();

        if($name) {
            $searchString = \quoma\core\helpers\StringHelper::normalName($name ? $name : '');

            //Para ordenar por relevancia, dado que también buscamos en el padre
            $query->select("location.*, MATCH (location.name) AGAINST ('$searchString' IN BOOLEAN MODE) AS relevance");

            if($type) {
                $query->andWhere(['location.type' => $type]);
            }

            $query->andFilterWhere(['location.status' => 'enabled']);

            //Buscamos con match en el padre
            $query->innerJoin(static::tableName().' parent', 'parent.location_id = location.parent_id');
            $query->andWhere(['or', "MATCH (location.name) AGAINST ('$searchString*' IN BOOLEAN MODE)", "MATCH (parent.name) AGAINST ('$searchString*' IN BOOLEAN MODE)"]);

            return $query->limit(50)->orderBy('relevance DESC')->all();
        }

        return $query->limit(50)->all();


    }

    /**
     * Actualiza el arbol de zonas.
     *
     * @param int $parent_id
     * @param int $left
     * @return int
     * @internal param int|null $parent_account_id
     */
    public static function updateTree($parent_id = 0, $left = 0)
    {
        $right = $left + 1;
        $query = Location::find()
                ->where(['parent_id' => $parent_id]);
        if ($parent_id == 0) {
            $query->orWhere(['parent_id' => null]);
        }
        $locations = $query->all();

        foreach ($locations as $location) {
            $right = Location::updateTree($location->location_id, $right);
        }
        Yii::$app->db->createCommand('update location set lft=' . $left . ", rgt=" . $right . " WHERE location_id=" . $parent_id)->execute();

        return $right + 1;
    }

    public static function getListForFilter()
    {
        $rows = self::find()->orderBy('name')->all();

        $result = [];
        foreach ($rows as $row) {
            $result[$row->location_id] = $row['name'] . ' (' . \quoma\arya\zone\ZoneModule::t(ucfirst($row['type'])) . ')';
        }

        return $result;
    }

}
