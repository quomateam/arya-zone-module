<?php
/**
 * Created by PhpStorm.
 * User: cgarcia
 * Date: 27/12/16
 * Time: 16:47
 */

namespace quoma\arya\zone\models;

use quoma\arya\zone\ZoneModule;

/**
 * This is the model class for table "zone".
 *
 * @property integer $zone_id
 * @property string $name
 *
 * @property ZoneMember[] $zoneMembers
 */
class Zone extends \quoma\core\db\ActiveRecord
{


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zone';
    }

    /**
     * @inheritdoc
     */
    /*
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['timestamp'],
                ],
            ],
            'date' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['date'],
                ],
                'value' => function(){return date('Y-m-d');},
            ],
            'time' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['time'],
                ],
                'value' => function(){return date('h:i');},
            ],
        ];
    }
    */

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'zone_id' => ZoneModule::t('ID'),
            'name' => ZoneModule::t('Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZoneMembers()
    {
        return $this->hasMany(ZoneMember::className(), ['zone_id' => 'zone_id']);
    }

    /**
     * @inheritdoc
     * Strong relations: None.
     */
    public function getDeletable()
    {
        return true;
    }

    /**
     * @brief Deletes weak relations for this model on delete
     * Weak relations: None.
     */
    protected function unlinkWeakRelations()
    {
        $this->unlinkAll('zoneMembers', true);
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            if($this->getDeletable()){
                $this->unlinkWeakRelations();
                return true;
            }
        } else {
            return false;
        }
    }

    /**
     * Busca Zona por nombre
     * @param $name
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function searchByName($name) {
        $query = Zone::find();

        $query->filterWhere(['like', 'name', $name]);

        return $query->all();
    }
}