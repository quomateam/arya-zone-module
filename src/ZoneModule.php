<?php

namespace quoma\arya\zone;

use quoma\core\menu\Menu;
use quoma\core\module\QuomaModule;

class ZoneModule extends QuomaModule
{
    public $controllerNamespace = 'quoma\arya\zone\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here

        $this->modules = [
            'api' => [
                'class' => 'quoma\arya\zone\modules\api\ZoneApiModule',
            ],
        ];

        $this->registerTranslations();
        $this->loadParams();
    }

    public function getMenu(Menu $menu)
    {
        $_menu = (new Menu(Menu::MENU_TYPE_ROOT))
            ->setName('zone')
            ->setLabel(self::t('Zones'))
            ->setSubItems([
                (new Menu(Menu::MENU_TYPE_ITEM))->setLabel(self::t('Locations'))->setUrl(['/zone/location/index']),
                (new Menu(Menu::MENU_TYPE_ITEM))->setLabel(self::t('Zones'))->setUrl(['/zone/zone/index']),
            ])
        ;
        $menu->addItem($_menu, Menu::MENU_POSITION_LAST);
        return $_menu;
    }

    public function getDependencies()
    {
        return [
        ];
    }
}
