<?php

use quoma\arya\zone\models\Location;
use yii\db\Migration;

class m200204_170909_add_CABA_location extends Migration
{

    public function up()
    {
        $provincia_buenos_aires_id = $this->db->createCommand("SELECT `location_id` FROM `location` WHERE `name` = 'Ciudad De Buenos Aires' AND `type` = 'state' LIMIT 1")->queryScalar();

        if($provincia_buenos_aires_id) {
            $this->insert('location', [
                'name' => 'Ciudad Autónoma de Buenos Aires - CABA',
                'type' => 'state',
                'parent_id' => $this->db->createCommand("SELECT `location_id` FROM `location` WHERE `name` = 'Argentina'")->queryScalar(),
                'status' => 'enabled',
                'system' => 'ciudad-autonoma-de-buenos-aires-caba'
            ]);

            $caba_id = $this->db->lastInsertID;

            foreach (Location::find()->where(['parent_id' => $provincia_buenos_aires_id, 'type' => 'neighborhood'])->all() as $barrio) {
                $barrio->updateAttributes(['parent_id' => $caba_id]);
            }
        }
    }

    public function down()
    {
        $provincia_buenos_aires_id = $this->db->createCommand("SELECT `location_id` FROM `location` WHERE `name` = 'Ciudad De Buenos Aires' AND `type` = 'state' LIMIT 1")->queryScalar();

        $caba_id = $this->db->createCommand("SELECT `location_id` FROM `location` WHERE `name` = 'Ciudad Autónoma de Buenos Aires - CABA' AND `type` = 'state' LIMIT 1")->queryScalar();

        foreach (Location::find()->where(['parent_id' => $caba_id, 'type' => 'neighborhood'])->all() as $barrio) {
            $barrio->updateAttributes(['parent_id' => $provincia_buenos_aires_id]);
        }
    }

}
