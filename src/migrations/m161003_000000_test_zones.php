<?php

use yii\db\Migration;

class m161003_000000_test_zones extends Migration
{

    public function up()
    {
        if (YII_ENV_PROD) {
            return true;
        }

        $this->execute("
            INSERT INTO `zone` (`zone_id`, `name`) VALUES
            (1, 'Este'),
            (2, 'Oeste'),
            (3, 'Norte'),
            (4, 'Sur'),
            (5, 'Centro');

        ");
    }

    public function down()
    {
        // nothing to do
    }

}
