<?php

use yii\db\Migration;

class m161002_000000_default_locations extends Migration
{
    public function up()
    {
        $this->execute("
            INSERT INTO `location` (`location_id`, `name`, `parent_id`, `create_timestamp`, `update_timestamp`, `status`, `system`, `type`, `postal_code`, `lft`, `rgt`) VALUES
            (1, 'Argentina', NULL, NULL, NULL, 'enabled', 'argentina', 'country', NULL, 1, 48),
            (2, 'Chile', NULL, NULL, NULL, 'enabled', 'chile', 'country', NULL, 49, 50),
            (3, 'Brasil', NULL, NULL, NULL, 'enabled', 'brasil', 'country', NULL, 51, 52),
            (4, 'Uruguay', NULL, NULL, NULL, 'enabled', 'uruguay', 'country', NULL, 53, 54),
            (5, 'Bolivia', NULL, NULL, NULL, 'enabled', 'bolivia', 'country', NULL, 55, 56),
            (6, 'Paraguay', NULL, NULL, NULL, 'enabled', 'paraguay', 'country', NULL, 57, 58),
            (7, 'Mendoza', 1, NULL, NULL, 'enabled', 'mendoza', 'state', NULL, 2, 3),
            (8, 'Jujuy', 1, NULL, NULL, 'enabled', 'jujuy', 'state', NULL, 4, 5),
            (9, 'Salta', 1, NULL, NULL, 'enabled', 'salta', 'state', NULL, 6, 7),
            (10, 'Catamarca', 1, NULL, NULL, 'enabled', 'catamarca', 'state', NULL, 8, 9),
            (11, 'La Rioja', 1, NULL, NULL, 'enabled', 'la-rioja', 'state', NULL, 10, 11),
            (12, 'San Juan', 1, NULL, NULL, 'enabled', 'san-juan', 'state', NULL, 12, 13),
            (13, 'San Luis', 1, NULL, NULL, 'enabled', 'san-luis', 'state', NULL, 14, 15),
            (14, 'Rio Negro', 1, NULL, NULL, 'enabled', 'rio-negro', 'state', NULL, 16, 17),
            (15, 'Chubut', 1, NULL, NULL, 'enabled', 'chubut', 'state', NULL, 18, 19),
            (16, 'Santa Cruz', 1, NULL, NULL, 'enabled', 'santa-cruz', 'state', NULL, 20, 21),
            (17, 'Tierra del Fuego', 1, NULL, NULL, 'enabled', 'tierra-del-fuego', 'state', NULL, 22, 23),
            (18, 'Buenos Aires', 1, NULL, NULL, 'enabled', 'buenos-aires', 'state', NULL, 24, 25),
            (19, 'Entre Ríos', 1, NULL, NULL, 'enabled', 'entre-rios', 'state', NULL, 26, 27),
            (20, 'Corrientes', 1, NULL, NULL, 'enabled', 'corrientes', 'state', NULL, 28, 29),
            (21, 'Misiones', 1, NULL, NULL, 'enabled', 'misiones', 'state', NULL, 30, 31),
            (22, 'Formosa', 1, NULL, NULL, 'enabled', 'formosa', 'state', NULL, 32, 33),
            (23, 'Chaco', 1, NULL, NULL, 'enabled', 'chaco', 'state', NULL, 34, 35),
            (24, 'Tucumán', 1, NULL, NULL, 'enabled', 'tucuman', 'state', NULL, 36, 37),
            (25, 'Santiago del Estero', 1, NULL, NULL, 'enabled', 'santiago-del-estero', 'state', NULL, 38, 39),
            (26, 'Córdoba', 1, NULL, NULL, 'enabled', 'cordoba', 'state', NULL, 40, 41),
            (27, 'La Pampa', 1, NULL, NULL, 'enabled', 'la-pampa', 'state', NULL, 42, 43),
            (28, 'Neuquén', 1, NULL, NULL, 'enabled', 'neuquen', 'state', NULL, 44, 45),
            (29, 'Santa Fe', 1, NULL, NULL, 'enabled', 'santa-fe', 'state', NULL, 46, 47);
            ");
    }

    public function down()
    {
    }

}
