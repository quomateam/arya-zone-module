<?php

use yii\db\Migration;

class m161001_000000_zone_tables extends Migration
{
    public function up()
    {
        $this->execute('
            CREATE TABLE location
            (
              location_id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
              name VARCHAR(255) NOT NULL,
              parent_id INT(11),
              create_timestamp INT(11),
              update_timestamp INT(11),
              status ENUM(\'enabled\', \'disabled\'),
              system VARCHAR(100),
              type VARCHAR(60),
              postal_code INT(11),
              lft INT(11),
              rgt INT(11),
              CONSTRAINT fk_location_location1 FOREIGN KEY (parent_id) REFERENCES location (location_id)
            );');
        $this->execute('CREATE INDEX fk_location_1_idx ON location (parent_id);');


        $this->execute('CREATE TABLE zone(
          zone_id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
          name VARCHAR(255) NOT NULL
          );');
        $this->execute("CREATE TABLE zone_member(
            zone_member_id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
            location_id INT(11),
            zone_id INT(11) NOT NULL,
            CONSTRAINT fk_zone_member_location FOREIGN KEY (location_id) REFERENCES location (location_id),
            CONSTRAINT zone_member_zone_zone_id_fk FOREIGN KEY (zone_id) REFERENCES zone (zone_id)
        );");
        $this->execute("CREATE INDEX fk_zone_member_location_idx ON zone_member (location_id);");
        $this->execute("CREATE INDEX zone_member_zone_zone_id_fk_idx ON zone_member (zone_id);");

    }

    public function down()
    {
        echo "m161223_110210_zone_tables cannot be reverted.\n";

        return false;
    }

}
