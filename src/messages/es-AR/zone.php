<?php return array (
    'Zone' => 'Zona',
    'Zones' => 'Zonas',
    'Country' => 'País',
    'State' => 'Provincia',
    'Department' => 'Departamento',
    'Locality' => 'Localidad/Distrito',
    'Location' => 'Ubicación',
    'Locations' => 'Ubicaciones',
    'Neighborhood' => 'Barrio',
    'Enabled' => 'Disponible',
    'Disabled' => 'Inactivo',
    'Type' => 'Tipo',
    'Parent' => 'Padre',
    'Postal Code' => 'Código Postal',
    'Name' => 'Nombre',
    'Status' => 'Estado',
    'Members' => 'Miembros'
);

