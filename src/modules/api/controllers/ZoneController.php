<?php

namespace quoma\arya\zone\modules\api\controllers;

use quoma\arya\zone\models\search\ZoneSearch;
use quoma\arya\zone\models\Zone;
use quoma\core\web\RestController;
use Yii;
use yii\data\ActiveDataProvider;

class ZoneController extends RestController
{
    public $modelClass = 'quoma\arya\zone\models\Zone';
    
    /**
     * @inheritdoc
     */
    public function getMethods()
    {
        return ['index'];
    }

    /**
     * @inheritdoc
     */
    public function getDisabledDefaultActions()
    {
        return ['delete', 'update', 'index'];
    }

    /**
     * Lists all models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new ZoneSearch();
        $searchModel->name         = Yii::$app->request->getQueryParam('name');
        $searchModel->zone_id      = Yii::$app->request->getQueryParam('zone_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $searchModel->search([])
        ]);

        $dataProvider->pagination = false;
        
        return $dataProvider;
    }
}
