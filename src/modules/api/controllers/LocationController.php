<?php

namespace quoma\arya\zone\modules\api\controllers;

use quoma\arya\zone\models\search\LocationSearch;
use quoma\core\web\RestController;
use Yii;
use yii\data\ActiveDataProvider;

class LocationController extends RestController
{
    public $modelClass = 'quoma\arya\zone\models\Location';

    /**
     * @inheritdoc
     */
    public function getMethods()
    {
        return ['index'];
    }

    /**
     * @inheritdoc
     */
    public function getDisabledDefaultActions()
    {
        return ['delete', 'update', 'index'];
    }
    
    /**
     * Lists all models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new LocationSearch();
        $searchModel->name              = Yii::$app->request->getQueryParam('name');
        $searchModel->location_id       = Yii::$app->request->getQueryParam('location_id');

        $dataProvider = $searchModel->search([]);

        $dataProvider->pagination = false;
        
        return $dataProvider;
    }
}
