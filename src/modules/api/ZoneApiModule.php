<?php

namespace quoma\arya\zone\modules\api;

class ZoneApiModule extends \yii\base\Module
{
    public $controllerNamespace = 'quoma\arya\zone\modules\api\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
