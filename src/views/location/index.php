<?php

use yii\helpers\Html;
use yii\grid\GridView;
use quoma\arya\zone\models\Location;

/* @var $this yii\web\View */
/* @var $searchModel quoma\arya\zone\models\search\ZoneSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = \quoma\arya\zone\ZoneModule::t('Locations');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zone-index">

    <div class="title">
        <h1><?= Html::encode($this->title) ?></h1>

        <p>
            <?= Html::a("<span class='glyphicon glyphicon-plus'></span> " . Yii::t('app', 'Create {modelClass}', ['modelClass' => \quoma\arya\zone\ZoneModule::t('Location')]), ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    </div>


    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => ['class' => 'table-responsive'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'name',
                'value' => function($model, $key, $index, $column) {

                    $searchModel = $column->grid->filterModel;
                    if ($searchModel->isFiltered()) {
                        return $model->name;
                    }

                    if (empty($model->parent_id)) {
                        return "<strong>$model->name</strong>";
                    } else {
                        return $model->tabName;
                    }
                },
                'format' => 'html'
            ],
            [
                'attribute' => 'type',
                //'filter'=>Yii::$app->params['type_location'],
                'value' => function($model) {
                    return quoma\arya\zone\ZoneModule::t(ucfirst($model->type));
                },
                'header' => \quoma\arya\zone\ZoneModule::t('Type'),
            ],
            [
                'attribute' => 'parent_id',
                'value' => function($model) {
                    return $model->parent ? $model->parent->name : '';
                },
                'filter' => \kartik\widgets\Select2::widget([
                    'name' => 'LocationSearch[parent_id]',
                    'value' => $searchModel->parent_id,
                    'data' => Location::getListForFilter(),
                    'options' => ['placeholder' => '', 'class' => 'filter'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]),
                'options' => [
                    'width' => '300px',
                ],
                'header' => \quoma\arya\zone\ZoneModule::t('Parent'),
            ],
            [
                'attribute' => 'postal_code',
                'value' => function($model) {
                    return $model->postal_code ? $model->postal_code : '';
                },
            ],
            [
                'attribute' => 'status',
                'filter' => [
                    'enabled' => Yii::t('app', 'Enabled'),
                    'disabled' => Yii::t('app', 'Disabled'),
                ],
                'value' => function($model) {
                    return Yii::t('app', ucfirst($model->status));
                }
            ],
            [
                'class' => 'quoma\core\grid\ActionColumn',
            ],
        ],
    ]);
    ?>

</div>
