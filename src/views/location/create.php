<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model quoma\arya\zone\models\Zone */

$this->title = Yii::t('app', 'Create');
$this->params['breadcrumbs'][] = ['label' => \quoma\arya\zone\ZoneModule::t('Locations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zone-create">

    <div class="row">
    	<div class="col-sm-8 col-sm-offset-2">
		    <h1><?= Html::encode($this->title) ?></h1>

		    <?= $this->render('_form', [
		        'model' => $model,
		    ]) ?>
    	</div>
    </div>

</div>
