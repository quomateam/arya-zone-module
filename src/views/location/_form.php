<?php

use quoma\arya\zone\models\Location;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $model Location */
/* @var $form ActiveForm */
?>

<div class="location-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'type')->label(\quoma\arya\zone\ZoneModule::t('Type'))
        ->dropDownList([
            'country' => \quoma\arya\zone\ZoneModule::t('Country'),
            'state' => \quoma\arya\zone\ZoneModule::t('State'),
            'department' => \quoma\arya\zone\ZoneModule::t('Department'),
            'locality' => \quoma\arya\zone\ZoneModule::t('Locality'),
            'neighborhood' => \quoma\arya\zone\ZoneModule::t('Neighborhood'),
        ], ['prompt' => \quoma\arya\zone\ZoneModule::t('Select')]) ?>

    <?=
    $form->field($model, 'parent_id')->widget(kartik\widgets\Select2::className(), [
        'data' => ArrayHelper::map(Location::getForSelect($model), 'location_id', 'name'),
        'options' => ['placeholder' => Yii::t("app", "Select"), 'encode' => false],
        'pluginOptions' => [
            'allowClear' => true
        ]
    ])
    ?>

    <?= $form->field($model, 'status')->dropDownList([ 'enabled' => \quoma\arya\zone\ZoneModule::t('Enabled'), 'disabled' => \quoma\arya\zone\ZoneModule::t('Disabled'),]) ?>

    <?= $form->field($model, 'postal_code')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
