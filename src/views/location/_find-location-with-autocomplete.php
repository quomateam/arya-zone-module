<?php

use quoma\arya\zone\models\Location;
use kartik\widgets\Select2;
use yii\helpers\Url;
use yii\web\JsExpression;

$location_id = $model->location_id;

$location = Location::findOne(['location_id'=> $location_id]);
$name = empty($location) ? '' : $location->getFullLocation();

if (!isset($clear)) {
    $clear = true;
}

//Plugin options
$pluginOptions = [
    'allowClear' => $clear,
    'minimumInputLength' => 3,
    'ajax' => [
        'url' => Url::to(['/zone/location/location-by-name']),
        'dataType' => 'json',
        'data' => new JsExpression('function(params) { return {name:params.term}; }')
    ],
    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
    'templateResult' => new JsExpression('function(zone) { return zone.text; }'),
    'templateSelection' => new JsExpression('function (zone) { return zone.text; }'),
    'cache' => true
];

//Con ActiveForm
if(isset($form)){
    echo $form->field($model, 'location_id')->label(isset($label) ? $label : null)->widget(Select2::classname(), [
        'data' => [$location_id => $name],
        'options' => ['placeholder' => Yii::t('app', 'Search')],
        'pluginOptions' => $pluginOptions,
    ]);
    
//Sin ActiveForm
}else{
    echo Select2::widget([
        'model' => $model,
        'attribute' => $location_id,
        'data' => [$location_id => $name],
        'options' => ['placeholder' => Yii::t('app', 'Search')],
        'pluginOptions' => $pluginOptions,
    ]);
}

?>
