<?php

use yii\helpers\Html;
use yii\grid\GridView;
use quoma\arya\zone\models\Location;

/* @var $this yii\web\View */
/* @var $searchModel quoma\arya\zone\models\search\ZoneSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = \quoma\arya\zone\ZoneModule::t('Zones');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zone-index">

    <div class="title">
        <h1><?= Html::encode($this->title) ?></h1>

        <p>
            <?= Html::a("<span class='glyphicon glyphicon-plus'></span> " . Yii::t('app', 'Create {modelClass}', ['modelClass' => \quoma\arya\zone\ZoneModule::t('Zone')]), ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'options' => ['class' => 'table-responsive'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute'=>'name',
                'format'=>'html'
            ],
            [
                'class' => 'quoma\core\grid\ActionColumn',
            ],
            
        ],
    ]); ?>

</div>
