<?php

use quoma\arya\zone\models\Zone;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $model Zone */
/* @var $form ActiveForm */
?>

<div class="zone-form">

    <?php $form = ActiveForm::begin([
        'id'=>'zone-form']); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>


    <?php ActiveForm::end();

    if(isset($membersDataProvider)) {
    ?>
    <div class="panel panel-primary">
        <div class="panel-heading" data-toggle="collapse" data-target="#panel-body-items" aria-expanded="true" aria-controls="panel-body-items">
            <h3 class="panel-title"><?= \quoma\arya\zone\ZoneModule::t('Members') ?></h3>
        </div>
        <div class="panel-body collapse in" id="panel-body-items" aria-expanded="true">
            <?php
            // Formulario para los miembros

            yii\widgets\Pjax::begin(['id' => 'members']);
            $formMember = ActiveForm::begin([
                'id'=>'add-member-form',
                'action' => ['add-member', 'id' => $model->zone_id],
                'options' => ['data-pjax' => true ]]);
                $member = new \quoma\arya\zone\models\ZoneMember();
            ?>

            <input type="hidden" name="ZoneMember[zone_id]" value="<?=$model->zone_id?>"/>

            <div class="row">
                <div class="col-sm-9 col-md-12">
                    <?= $this->render('@zone/views/location/_find-location-with-autocomplete', ['model' => $member, 'form' => $formMember, 'clear' => false]) ?>
                </div>

                <div class="col-sm-9 col-md-2">
                    <label style="display: block">&nbsp;</label>
                    <a class="btn btn-primary" id="member-add">
                        <span class="glyphicon glyphicon-plus"></span> <?= Yii::t('app', 'Add') ?>
                    </a>
                </div>
            </div>

            <?php
            ActiveForm::end();

            echo \yii\grid\GridView::widget([
                'id'=>'grid',
                'dataProvider' => $membersDataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'header'=>\quoma\arya\zone\ZoneModule::t('Name'),
                        'value'=>function($model){
                            return $model->location->name;
                        },
                    ],
                    [
                        'value'=>function($model){
                            return \quoma\arya\zone\ZoneModule::t(ucfirst($model->location->type));
                        },
                        'header'=>\quoma\arya\zone\ZoneModule::t('Type'),
                    ],
                    [
                        'value'=>function($model){
                            return $model->location->parent ? $model->location->parent->name : '';
                        },
                        'header'=>\quoma\arya\zone\ZoneModule::t('Parent')
                    ],
                    [
                        'class' => 'quoma\core\grid\ActionColumn',
                        'template'=>'{delete}',
                        'buttons'=>[
                            'delete'=>function ($url, $model, $key) {
                                return '<a class="removeMember btn btn-danger" href="#" data-url="'.\yii\helpers\Url::toRoute(['zone/delete-member', 'zone_id'=>$model->zone_id, 'zone_member_id'=>$model->zone_member_id]).
                                    '" title="'.Yii::t('app','Delete').'" data-confirm-text="'.Yii::t('yii','Are you sure you want to delete this item?').'" ><span class="glyphicon glyphicon-trash"></span></a>';
                            }
                        ]
                    ],
                ],
                'options'=>[
                    'style'=>'margin-top:10px;'
                ]
            ]);
            ?>
            <?php \yii\widgets\Pjax::end();?>
        </div>
    </div>
    <?php } ?>

    <div class="form-group">
        <a id="saveBill" onclick="Zone.save();" class="btn <?=($model->isNewRecord ? 'btn btn-success' : 'btn btn-primary')?>">
            <!-- <span class='glyphicon glyphicon-plus'></span>              -->
            <?= ($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update')) ?>
        </a>
    </div>

</div>
    <script>
        var Zone = new function() {
            this.init = function () {
                // Eventos de impuestos
                $(document).on("click","#member-add", function(){
                    Zone.addMember();
                });
                $("#new_tax").on("pjax:end", function() {
                    $.pjax.reload({container:"#members", async:false});
                });

                $(document).on("click",".removeMember", function(event){
                    Zone.removeMember(this, event);
                });
            }

            this.addMember = function() {
                $('#add-member-form').submit();
            }

            this.removeMember = function(elem, event) {
                event.preventDefault();
                var url = $(elem).data('url');
                var confirmText = $(elem).data('confirm-text');
                if (confirm(confirmText)) {
                    $.ajax({
                        url: url,
                        type: 'post',
                    }).done(function(data) {
                        $.pjax.reload({container:"#members", async:false});
                    });
                }
            }

            this.save = function()
            {
                $('#zone-form').submit();
            }
        }
    </script>
<?php  $this->registerJs("Zone.init();"); ?>